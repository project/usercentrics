<?php

namespace Drupal\usercentrics\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete a Usercentrics app.
 */
class UsercentricsAppDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the Usercentrics Data Processing Service "%name?"', [
      '%name' => $this->entity->label(),
    ], ['context' => 'usercentrics']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('usercentrics.admin.order_form');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete', [], ['context' => 'usercentrics']);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()->addMessage($this->t('Usercentrics Data Processing Service %label has been deleted.', [
      '%label' => $this->entity->label(),
    ], ['context' => 'usercentrics']));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
