<?php

namespace Drupal\usercentrics\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\usercentrics\UsercentricsAppInterface;

/**
 * Defines the Usercentrics App config entity.
 *
 * @ingroup usercentrics
 *
 * @ConfigEntityType(
 *   id = "usercentrics_app",
 *   label = @Translation("Usercentrics Data Processing Service (DPS)"),
 *   label_singular = @Translation("Usercentrics Data Processing Service (DPS)"),
 *   label_plural = @Translation("Usercentrics Data Processing Services"),
 *   handlers = {
 *     "list_builder" = "Drupal\usercentrics\UsercentricsAppListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *     "form" = {
 *       "add" = "Drupal\usercentrics\Form\UsercentricsAppForm",
 *       "edit" = "Drupal\usercentrics\Form\UsercentricsAppForm",
 *       "delete" = "Drupal\usercentrics\Form\UsercentricsAppDeleteForm",
 *     },
 *   },
 *   links = {
 *     "collection" = "/admin/config/user-interface/usercentrics/apps",
 *     "add-form" = "/admin/config/user-interface/usercentrics/apps/add",
 *     "edit-form" = "/admin/config/user-interface/usercentrics/apps/{usercentrics_app}",
 *     "delete-form" = "/admin/config/user-interface/usercentrics/apps/{usercentrics_app}/delete"
 *   },
 *   admin_permission = "administer usercentrics",
 *   entity_keys = {
 *     "id" = "id",
 *     "uc_id" = "uc_id",
 *     "status" = "status",
 *     "label" = "label",
 *     "javascripts" = "javascripts",
 *     "attachments" = "attachments",
 *     "libraries" = "libraries",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "uc_id",
 *     "status",
 *     "label",
 *     "javascripts",
 *     "attachments",
 *     "libraries",
 *     "weight",
 *   },
 * )
 */
class UsercentricsApp extends ConfigEntityBase implements UsercentricsAppInterface {

  /**
   * Machine name of the app.
   *
   * @var string
   */
  protected $id;

  /**
   * Usercentrics ID of the app.
   *
   * @var string
   */
  protected $uc_id;

  /**
   * The label of the app.
   *
   * @var string
   */
  protected $label;

  /**
   * The javascripts that will added to the DOM as text/html instead.
   *
   * The values will match against a string comparison of the "src"-attributes
   * of script, iframe, img, audio and video tags.
   *
   * @var array
   */
  protected $javascripts = [];

  /**
   * The attachment identifiers that will get manipulated.
   *
   * The values will exactly match the hook_attachment keys.
   *
   * @var array
   */
  protected $attachments = [];

  /**
   * The libraries identifiers that will get manipulated.
   *
   * The values will exactly match the hook_libraries_alter keys.
   *
   * @var array
   */
  protected $libraries = [];

  /**
   * The weight of the app.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function id(): ?string {
    return $this->get('id');
  }

  /**
   * {@inheritdoc}
   */
  public function setId(string $id): UsercentricsAppInterface {
    return $this->set('id', $id);
  }

  /**
   * {@inheritdoc}
   */
  public function ucId(): ?string {
    return $this->get('uc_id');
  }

  /**
   * {@inheritdoc}
   */
  public function setUcId(string $uc_id): UsercentricsAppInterface {
    return $this->set('uc_id', $uc_id);
  }

  /**
   * {@inheritdoc}
   */
  public function label(): ?string {
    return $this->get('label');
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label): UsercentricsAppInterface {
    return $this->set('label', $label);
  }

  /**
   * {@inheritdoc}
   */
  public function attachments(): array {
    return $this->get('attachments');
  }

  /**
   * {@inheritdoc}
   */
  public function setAttachments(array $attachments = []): UsercentricsAppInterface {
    return $this->set('attachments', $attachments);
  }

  /**
   * {@inheritdoc}
   */
  public function javascripts(): array {
    return $this->get('javascripts');
  }

  /**
   * {@inheritdoc}
   */
  public function setJavaScripts(array $javascripts = []): UsercentricsAppInterface {
    return $this->set('javascripts', $javascripts);
  }

  /**
   * {@inheritdoc}
   */
  public function libraries(): array {
    return $this->get('libraries');
  }

  /**
   * {@inheritdoc}
   */
  public function setLibraries(array $libraries = []): UsercentricsAppInterface {
    return $this->set('libraries', $libraries);
  }

  /**
   * {@inheritdoc}
   */
  public function weight(): int {
    return $this->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight(int $weight = 0): UsercentricsAppInterface {
    return $this->set('weight', $weight);
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    // Invalidate cache.
    Cache::invalidateTags(['config:usercentrics.settings', 'library_info']);

    return parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    // Invalidate cache.
    Cache::invalidateTags(['config:usercentrics.settings', 'library_info']);

    return parent::delete();
  }

}
