<?php

namespace Drupal\usercentrics\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\usercentrics\Utility\UsercentricsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add form for a Usercentrics App.
 *
 * @internal
 */
class UsercentricsAppForm extends EntityForm {

  /**
   * The Usercentrics helper service.
   *
   * @var \Drupal\usercentrics\Utility\UsercentricsHelper
   */
  protected $usercentricsHelper;

  /**
   * Constructs an Form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   * @param \Drupal\usercentrics\Utility\UsercentricsHelper $usercentrics_helper
   *   The Usercentrics helper service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, UsercentricsHelper $usercentrics_helper) {
    $this->entityTypeManager = $entityTypeManager;
    $this->usercentricsHelper = $usercentrics_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('usercentrics.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\usercentrics\UsercentricsAppInterface $app */
    $app = $this->entity;
    $form = parent::form($form, $form_state);
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label', [], ['context' => 'usercentrics']),
      '#maxlength' => 255,
      '#default_value' => $app->label(),
      '#description' => $this->t("The label for this Data Processing Service must be equal to Usercentrics Data Processing Service (DPS) name.", [], ['context' => 'usercentrics']),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $app->id(),
      '#description' => $this->t('A unique machine-readable name for this Usercentrics app.', [], ['context' => 'usercentrics']),
      '#maxlength' => 32,
      '#machine_name' => [
        'exists' => [$this, 'exist'],
        'source' => ['label'],
      ],
      '#disabled' => !$app->isNew(),
      '#required' => TRUE,
    ];

    $form['uc_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Usercentrics Template ID', [], ['context' => 'usercentrics']),
      '#default_value' => $app->ucId(),
      '#description' => $this->t('The Usercentrics Data Processing Service (DPS) Template ID (for documentation purposes only).', [], ['context' => 'usercentrics']),
      '#maxlength' => 32,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled', [], ['context' => 'usercentrics']),
      '#default_value' => $app->isNew() ? FALSE : $app->status(),
    ];

    $form['js'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Sources', [], ['context' => 'usercentrics']),
      '#description' => $this->t('As they appear in the src attribute of script, iframe, img, video and audio tags, Enter one source per line, partial matches are supported.', [], ['context' => 'usercentrics']),
      '#default_value' => implode("\n", $app->javascripts()),
      '#placeholder' => "modules/custom/mymodule/js/script.js\nhttps://example.org/script.js\ntracking.js",
    ];
    $form['att'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Attachments', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Some Javascript files are added as <em>page attachments</em> with a unique identifier. If Usercentrics should take control over these scripts, enter their IDs here, one per line.', [], ['context' => 'usercentrics']),
      '#default_value' => implode("\n", $app->attachments()),
    ];
    $form['libraries'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Libraries', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Add library names to control them by Usercentrics.', [], ['context' => 'usercentrics']),
      '#default_value' => implode("\n", $app->libraries()),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\usercentrics\UsercentricsAppInterface $app */
    $app = $this->entity;
    $app->setJavaScripts(array_filter(array_map('trim', explode("\n", $form_state->getValue('js')))));
    $app->setAttachments(array_filter(array_map('trim', explode("\n", $form_state->getValue('att')))));
    $app->setLibraries(array_filter(array_map('trim', explode("\n", $form_state->getValue('libraries')))));

    $status = $app->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The Usercentrics Data Processing Service %label has been created.', [
        '%label' => $app->label(),
      ], ['context' => 'usercentrics']));
    }
    else {
      $this->messenger()->addMessage($this->t('The Usercentrics Data Processing Service %label has been updated.', [
        '%label' => $app->label(),
      ], ['context' => 'usercentrics']));
    }

    $form_state->setRedirect('usercentrics.admin.order_form');

    return $status;
  }

  /**
   * Check if an Usercentrics App configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('usercentrics_app')->getQuery()
      ->condition('id', $id)
      ->accessCheck(FALSE)
      ->execute();
    return (bool) $entity;
  }

}
