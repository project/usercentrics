<?php

namespace Drupal\usercentrics;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface definition for a Usercentrics app config entity.
 */
interface UsercentricsAppInterface extends ConfigEntityInterface {

  /**
   * Getter for the id.
   *
   * @return string|null
   *   The Id.
   */
  public function id(): ?string;

  /**
   * Setter for the id.
   *
   * @param string $id
   *   The id.
   *
   * @return \Drupal\usercentrics\UsercentricsAppInterface
   *   The instance.
   */
  public function setId(string $id): UsercentricsAppInterface;

  /**
   * Getter for the usercentrics id.
   *
   * @return string|null
   *   The Id.
   */
  public function ucId(): ?string;

  /**
   * Setter for the usercentrics id.
   *
   * @param string $uc_id
   *   The usercentrics id.
   *
   * @return \Drupal\usercentrics\UsercentricsAppInterface
   *   The instance.
   */
  public function setUcId(string $uc_id): UsercentricsAppInterface;

  /**
   * Getter for the label.
   *
   * @return string|null
   *   The label.
   */
  public function label(): ?string;

  /**
   * Setter for the label.
   *
   * @param string $label
   *   The label.
   *
   * @return \Drupal\usercentrics\UsercentricsAppInterface
   *   The instance.
   */
  public function setLabel(string $label): UsercentricsAppInterface;

  /**
   * Getter for javascripts.
   *
   * @return array
   *   The javascripts.
   */
  public function javascripts(): array;

  /**
   * Setter for javascripts.
   *
   * @param array $javascripts
   *   The javascripts.
   *
   * @return \Drupal\usercentrics\UsercentricsAppInterface
   *   The instance.
   */
  public function setJavaScripts(array $javascripts = []): UsercentricsAppInterface;

  /**
   * Getter for attachments.
   *
   * @return array
   *   The attachments.
   */
  public function attachments(): array;

  /**
   * Setter for attachments.
   *
   * @param array $attachments
   *   The attachments.
   *
   * @return \Drupal\usercentrics\UsercentricsAppInterface
   *   The instance.
   */
  public function setAttachments(array $attachments = []): UsercentricsAppInterface;

  /**
   * Getter for libraries.
   *
   * @return array
   *   The libraries.
   */
  public function libraries(): array;

  /**
   * Setter for libraries.
   *
   * @param array $libraries
   *   The libraries.
   *
   * @return \Drupal\usercentrics\UsercentricsAppInterface
   *   The instance.
   */
  public function setLibraries(array $libraries = []): UsercentricsAppInterface;

  /**
   * Getter for the weight.
   *
   * @return int
   *   The weight.
   */
  public function weight(): int;

  /**
   * Setter for the weight.
   *
   * @param int $weight
   *   The weight.
   *
   * @return \Drupal\usercentrics\UsercentricsAppInterface
   *   The instance.
   */
  public function setWeight(int $weight = 0): UsercentricsAppInterface;

}
