<?php

namespace Drupal\usercentrics;

use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Core\Asset\JsCollectionRenderer;

/**
 * Renders JavaScript assets.
 */
class UsercentricsJsCollectionRenderer extends JsCollectionRenderer implements AssetCollectionRendererInterface {

  /**
   * {@inheritdoc}
   *
   * This class evaluates the aggregation enabled/disabled condition on a group
   * by group basis by testing whether an aggregate file has been made for the
   * group rather than by testing the site-wide aggregation setting. This allows
   * this class to work correctly even if modules have implemented custom
   * logic for grouping and aggregating files.
   */
  public function render(array $js_assets) {

    return array_map(function ($js_asset, $element) {
      if (isset($js_asset['usercentrics']) && !empty($js_asset['usercentrics'])) {
        $element['#attributes']['data-usercentrics'] = $js_asset['usercentrics'];
        $element['#attributes']['type'] = 'text/plain';
      }
      return $element;
    }, $js_assets, parent::render($js_assets));

  }

}
