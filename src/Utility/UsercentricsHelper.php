<?php

namespace Drupal\usercentrics\Utility;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides helper methods for Usercentrics.
 */
class UsercentricsHelper {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * If logging is enabled.
   *
   * @var bool
   */
  protected $logEnabled;

  /**
   * The AdminContext service.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Constructs a UsercentricsHelper object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The AdminContext service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    RendererInterface $renderer,
    LoggerChannelFactoryInterface $logger,
    AdminContext $admin_context,
  ) {
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request_stack->getCurrentRequest();
    $this->renderer = $renderer;
    $this->logger = $logger;
    $this->adminContext = $admin_context;

    $this->logEnabled = $this->getSettings()->get('debug');
  }

  /**
   * Returns if user has access to use Usercentrics.
   *
   * @return bool
   *   If has access in general.
   */
  public function hasAccess(): bool {
    if (!$this->currentUser->hasPermission('use usercentrics')) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns if all necessary dependencies are satisfied to use Usercentrics.
   *
   * @return bool
   *   If has access in general.
   */
  public function isEnabled(): bool {
    $config = $this->getSettings();
    $enabled = $config->get('enabled');
    $settings_id = $config->get('settings_id');

    // Check if enabled and settings ID set.
    if (!$enabled || empty($settings_id)) {
      return FALSE;
    }

    // Check for admin routes.
    if ($config->get('exclude_admin_paths') && ($this->adminContext->isAdminRoute())) {
      return FALSE;
    }

    // Check for admin role.
    if ($config->get('exclude_admin_role') && ($this->currentUser->isAuthenticated())) {
      // Check for admin roles for current user.
      // Write better solution if #3412578 is solved.
      $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
      $roles = $user->get('roles');
      foreach ($roles as $role) {
        if ($role->get('entity')->getTarget()->getEntity()->isAdmin()) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  /**
   * Gets usercentrics.settings config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The usercentrics settings.
   */
  public function getSettings(): ImmutableConfig {
    return $this->configFactory->get('usercentrics.settings');
  }

  /**
   * Gets drupal render service.
   *
   * @return \Drupal\Core\Render\RendererInterface
   *   The render service.
   */
  public function getRenderer(): RendererInterface {
    return $this->renderer;
  }

  /**
   * Convert under_score type array keys to camelCase type array keys.
   *
   * @param array $array
   *   The array to convert.
   *
   * @return array
   *   The array with converted keys.
   */
  private static function snakeToCamel(array $array): array {
    $finalArray = [];

    foreach ($array as $key => $value) {
      if (strpos($key, "_")) {
        $key = lcfirst(str_replace("_", "", ucwords($key, "_")));
      }
      if (!is_array($value)) {
        $finalArray[$key] = $value;
      }
      else {
        $finalArray[$key] = static::snakeToCamel($value);
      }
    }

    return $finalArray;
  }

  /**
   * Get all apps.
   *
   * @param bool $only_enabled
   *   If only enabled apps should be fetched.
   *
   * @return \Drupal\usercentrics\UsercentricsAppInterface[]
   *   The enabled apps.
   */
  public function getApps(bool $only_enabled = TRUE): array {
    $storage = $this->entityTypeManager->getStorage('usercentrics_app');

    $query = $storage->getQuery();
    $query->sort('weight');
    if ($only_enabled) {
      $query->condition('status', TRUE);
    }
    $result = $storage->loadMultiple($query->accessCheck(FALSE)->execute());

    return $result;
  }

  /**
   * Check if on disabled uri pattern.
   *
   * @return bool
   *   True or false.
   */
  public function onDisabledUri(): bool {
    $config = $this->configFactory->get('usercentrics.settings');
    $disable_urls = $config->get('disable_urls');

    // Disable media/oembed as the outer iframe of remote-video will be handled.
    $disable_urls[] = '^\/media\/oembed';

    $uri = $this->request->getRequestUri();
    $found = FALSE;
    foreach ($disable_urls as $url_pattern) {
      $pattern = '/' . $url_pattern . '/';
      if (preg_match($pattern, $uri) > 0) {
        $found = TRUE;
      }
    }
    return $found;
  }

  /**
   * Check if on excluded uri pattern.
   *
   * @return bool
   *   True or false.
   */
  public function onExcludedUri(): bool {
    $config = $this->configFactory->get('usercentrics.settings');
    $exclude_urls = $config->get('exclude_urls');
    if (empty($exclude_urls)) {
      return FALSE;
    }

    $uri = $this->request->getRequestUri();
    $found = FALSE;
    foreach ($exclude_urls as $url_pattern) {
      $pattern = '/' . $url_pattern . '/';
      if (preg_match($pattern, $uri) > 0) {
        $found = TRUE;
      }
    }
    return $found;
  }

  /**
   * Write debug info in watchdog.
   */
  public function debug($message, $variables = [], $level_info = FALSE) {
    if ($this->logEnabled) {
      $logLevel = $level_info ? RfcLogLevel::INFO : RfcLogLevel::DEBUG;
      $this->log($message, $variables, $logLevel);
    }
  }

  /**
   * Internal helper function for writing log..
   */
  protected function log($message, $variables = [], $severity = RfcLogLevel::DEBUG) {
    $this->logger->get('usercentrics')->log($severity, $message, $variables);
  }

}
