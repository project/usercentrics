<?php

namespace Drupal\usercentrics\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * General settings form for the Usercentrics consent manager.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typedConfigManager
   *   The typed config manager.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManagerInterface.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory, $typedConfigManager);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the services required to construct this class.
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'usercentrics_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'usercentrics.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('usercentrics.settings');

    $role_with_permission = FALSE;
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    foreach ($roles as $roleName => $role) {
      if ($roleName != 'administrator' && $role->hasPermission('use usercentrics')) {
        $role_with_permission = TRUE;
      }
    }
    if (!$role_with_permission) {
      $form['role_hint'] = [
        'message' => [
          '#theme' => 'status_messages',
          '#message_list' => [
            'warning' => [
              $this->t('Currently only the administrator role has the "use usercentrics" permission. To let the visitors of your site manager their consents with usercentrics, add the "use usercentrics" permission to role "anonymous"'),
            ],
          ],
          '#status_headings' => [
            'warning' => $this
              ->t('No permissions set'),
          ],
        ],
      ];
    }

    $form['vertical_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Usercentrics settings', [], ['context' => 'usercentrics']),
    ];

    // CMP settings.
    $form['cmp_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('CMP Settings', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Configure Usercentrics CMP.', [], ['context' => 'usercentrics']),
      '#group' => 'vertical_tabs',
    ];
    $form['cmp_settings']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Usercentrics CMP', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Uncheck this setting to disable Usercentrics CMP at all.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('enabled'),
    ];
    $form['cmp_settings']['settings_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Usercentrics ID', [], ['context' => 'usercentrics']),
      '#description' => $this->t('The Usercentrics ID.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('settings_id'),
    ];
    $form['cmp_settings']['tcf_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Transparency & Consent Framework', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Enable Transparency & Consent Framework (TCF).', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('tcf_enabled'),
    ];
    $form['cmp_settings']['preview'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Preview-Mode', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Use Preview-Mode', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('preview'),
    ];
    $form['cmp_settings']['disable_tracking'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Tracking', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Disable Tracking (e.g. for preview mode)', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('disable_tracking'),
    ];

    // SDP settings.
    $form['sdp_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('SDP Settings', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Configure Smart Data Protection.', [], ['context' => 'usercentrics']),
      '#group' => 'vertical_tabs',
    ];
    $form['sdp_settings']['sdp_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Smart Data Protection', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Enable Smart Data Protection (SDP). Usercentrics CMP must be enabled to use SDP.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('sdp_enabled'),
    ];

    // Button settings.
    $form['button_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Button Settings', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Configure CMP toggle button.', [], ['context' => 'usercentrics']),
      '#group' => 'vertical_tabs',
    ];
    $form['button_settings']['show_toggle_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show CMP toggle button', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Show CMP toggle button', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('show_toggle_button'),
    ];
    $form['button_settings']['toggle_button_icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Icon URL', [], ['context' => 'usercentrics']),
      '#description' => $this->t('URL for icon of Toggle Button. If unset default icon is used', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('toggle_button_icon'),
    ];

    // Advanced settings.
    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced Settings', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Advanced and debugging settings.', [], ['context' => 'usercentrics']),
      '#group' => 'vertical_tabs',
    ];
    $form['advanced_settings']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Debug Mode', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Enable debug mode to log in watchdog.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('debug'),
    ];
    $form['advanced_settings']['exclude_admin_paths'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude admin paths', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Exclude admin paths from CMP.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('exclude_admin_paths'),
    ];
    $form['advanced_settings']['exclude_admin_role'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude admin role', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Exclude admin role from CMP.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('exclude_admin_role'),
    ];
    $form['advanced_settings']['auto_decorate_js_alter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Process js_alter', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Matches and decorates script files added from libraries against the configured apps.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('auto_decorate_js_alter'),
    ];
    $form['advanced_settings']['auto_decorate_page_attachments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Process page_attachments', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Matches and decorates manually attached JS files against the configured apps.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('auto_decorate_page_attachments'),
    ];
    $form['advanced_settings']['auto_decorate_library_info_alter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Process library_info_alter', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Matches and decorates libraries against the configured apps.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('auto_decorate_library_info_alter'),
    ];
    $form['advanced_settings']['exclude_urls'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Disable Usercentrics and block attributed resources on following url patterns', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Enter one regular expression per line without delimiters, i.e  \/admin\/ will match all paths that contain /admin/ while i.e ^\/en will match all routes that start with /en. On these paths all resources remain blocked and Usercentrics will be disabled.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('exclude_urls') ? implode("\n", $config->get('exclude_urls')) : '',
    ];
    $form['advanced_settings']['disable_urls'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Disable Usercentrics element and do not block attributed resources on following url patterns', [], ['context' => 'usercentrics']),
      '#description' => $this->t('Enter one regular expression per line without delimiters, i.e  \/admin\/ will match all paths that contain /admin/ while i.e ^\/en will match all routes that start with /en. On these paths no resources are blocked and Usercentrics will be disabled.', [], ['context' => 'usercentrics']),
      '#default_value' => $config->get('disable_urls') ? implode("\n", $config->get('disable_urls')) : '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('usercentrics.settings');

    $exclude_urls = array_map('trim', explode("\n", $form_state->getValue('exclude_urls')));
    $disable_urls = array_map('trim', explode("\n", $form_state->getValue('disable_urls')));

    $config
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('settings_id', $form_state->getValue('settings_id'))
      ->set('tcf_enabled', $form_state->getValue('tcf_enabled'))
      ->set('preview', $form_state->getValue('preview'))
      ->set('disable_tracking', $form_state->getValue('disable_tracking'))
      ->set('sdp_enabled', $form_state->getValue('sdp_enabled'))
      ->set('show_toggle_button', $form_state->getValue('show_toggle_button'))
      ->set('toggle_button_icon', $form_state->getValue('toggle_button_icon'))
      ->set('debug', $form_state->getValue('debug'))
      ->set('exclude_admin_paths', $form_state->getValue('exclude_admin_paths'))
      ->set('exclude_admin_role', $form_state->getValue('exclude_admin_role'))
      ->set('auto_decorate_js_alter', $form_state->getValue('auto_decorate_js_alter'))
      ->set('auto_decorate_page_attachments', $form_state->getValue('auto_decorate_page_attachments'))
      ->set('auto_decorate_library_info_alter', $form_state->getValue('auto_decorate_library_info_alter'))
      ->set('exclude_urls', array_filter($exclude_urls))
      ->set('disable_urls', array_filter($disable_urls));

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
