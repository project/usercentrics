<?php

namespace Drupal\usercentrics;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Usercentrics apps.
 */
class UsercentricsAppListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label', [], ['context' => 'usercentrics']);
    $header['uc_id'] = $this->t('Usercentrics ID', [], ['context' => 'usercentrics']);
    $header['status'] = $this->t('Status', [], ['context' => 'usercentrics']);

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\usercentrics\UsercentricsAppInterface $entity */
    $row['label'] = $entity->label();
    $row['uc_id'] = $entity->ucId();
    $row['status'] = $entity->status() ? $this->t('Enabled', [], ['context' => 'usercentrics']) : $this->t('Disabled', [], ['context' => 'usercentrics']);

    return $row + parent::buildRow($entity);
  }

}
