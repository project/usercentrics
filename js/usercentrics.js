/**
 * @file
 * Attaches the behaviors for the Usercentrics module.
 */

(($, Drupal, window, once, drupalSettings) => {
  /**
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Adds behaviors for usercentrics module.
   */
  Drupal.behaviors.usercentrics = {
    attach(context) {
      const buttonLabel = Drupal.t(
        'Manage consents',
        {},
        { context: 'usercentrics' },
      );
      const buttonHtml = $(
        `<button id="uc_toggle_dialog" aria-label="${buttonLabel}" title="${buttonLabel}" type="button" class="uc_toggle_dialog uc_toggle_dialog_override" rel="open-consent-manager">${buttonLabel}</button>`,
      );
      $(buttonHtml).on('click', (event) => {
        event.preventDefault();
        /* global UC_UI */
        UC_UI.showFirstLayer();
      });
      if (
        typeof drupalSettings.usercentrics !== 'undefined' &&
        typeof drupalSettings.usercentrics.logoPath !== 'undefined'
      ) {
        buttonHtml.css(
          'background-image',
          `url(${drupalSettings.usercentrics.logoPath})`,
        );
      }
      $('body', context).append(buttonHtml);
    },
  };
})(jQuery, Drupal, this, once, drupalSettings);
