# Usercentrics CMP

This module implements the Consent Management Platform (CMP) from
usercentrics.com.

Users of the website must select which data processing services are to be used
before loading from external sources.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/usercentrics).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/usercentrics).

## Table of contents

 * Requirements
 * Installation
 * Configuration
 * How it works
 * oEmbed Media
 * Data Processing Service (DPS)

## Requirements

This module requires no modules outside of Drupal core.

## Recommended modules

[Media oEmbed Provider Markup](https://www.drupal.org/project/media_oembed_provider_markup):
If you want to use the Smart Data Protector (SDP), you need this module to
replaces internal Drupal media oEmbed URLs with provider's HTML markup. More see
section "oEmbed Media".

## Installation

Install as you would normally install a contributed Drupal module.

See https://www.drupal.org/docs/extending-drupal/installing-modules for
further information.

No other libraries are required.

## Configuration

1. Go to Administration » Configuration » User Interface » Usercentrics 

2. Enter you usercentrics ID and enable the service

Further options / features:

Smart Data Protection (SDP) can be activated in a separate tab.

You can display an icon at the bottom of the screen that users can use to
access their consent settings at any time. You can also configure your own
logo path for this button.

In the advanced settings tab, debugging settings can be configured to support
the creation of own Usercentrics Data Processing Services. Usercentrics can
also be deactivated for admin paths (default) or users with the admin role.

**To make usercentrics work, you have to enable Data Processing Services (DPS).
See section below**

## How it works

Usercentrics offers a service for managing consents, e.g. for the use of
cookies or 3rd party services.

To make this work, the script tags must be customized (see Usercentrics
documentation). The module makes these adjustments if the appropriate
Usercentrics Data Processing Services are configured in Drupal.


## oEmbed Media

To load the embed media, Drupal uses an iframe as the media source, the content
of which is loaded from /media/oembed. This ensures that the external content
is loaded in an iframe.

Due to the changed URL, Consent Manager Platforms (CMPs) do not recognize the
external source and cannot block it. This module embeds the original source of
the oEmbed provider.

To mitigate this, you can use the Drupal module media_oembed_provider_markup:
see https://www.drupal.org/project/media_oembed_provider_markup

## Data Processing Services (DPS)

To make the necessary changes to the script integrations the corresponding
DPSs must be configured and activated. The module provides apps for Matomo,
Matomo (self hosted), Google Analytics and Google Tag Manager.
You can enable them on /admin/config/user-interface/usercentrics.
All DPSs are disabled by default.

The configurations come without guarantee. The site operator is responsible
for testing the correct implementation.

- If you use Data Processing Service (DPS) Google Analytics 4 with the Google
Tags module for Drupal you can use the predefined "Google Analytics 4" DPS,
but you cannot use the predefined DPSs "Google Analytics 4" and "Google Tags"
at the same time.
- The Drupal module Google Analytics is deprecated and the successor is the
module Google Tag 2.0+.
- The Drupal module Google Analytics GA4 cannot be used with this Usercentrics
module, as it works with inline-themes and therefore bypasses Drupal logic.

You can create your own apps for the scripts you use.

The focus is on the information that the module can use to recognize and adapt
the associated scripts. This can be:

* a library name (see module.libraries.yml)
* the script name or a part of it (see e.g. in the source representation of
  your website)
* the machine name of an attachment (see source code of your module).

Debug mode can be activated to help with the creation. All libraries, scripts
and attachments found are then documented in the watchdog (severity debug);
if adjustments are made, this is logged with severity info.

Please check the correct implementation (e.g. with Firefox Inspector or
Lighthouse Project). Modules may change their code or configuration. Some
Modules may implement external source in other ways (e.g. iframe) so you have
to decide if this comply with your needs.
