<?php

namespace Drupal\usercentrics\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UsercentricsAppOrderForm for entity collection with order function.
 *
 * @package Drupal\usercentrics\Form
 */
class UsercentricsAppOrderForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
  ) {

    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'usercentrics_app_order_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get all the apps.
    $query = $this->entityTypeManager->getStorage('usercentrics_app')->getQuery();
    $results = $query->sort('label')->sort('weight')->accessCheck(FALSE)->execute();
    $apps = $this->entityTypeManager->getStorage('usercentrics_app')->loadMultiple($results);

    // Get list builder to get operations.
    $list_builder = $this->entityTypeManager->getListBuilder('usercentrics_app');

    $form['#tree'] = TRUE;
    $form['apps'] = [
      '#type' => 'table',
      '#empty' => $this->t('No apps have been created yet.'),
      '#title' => $this->t('Usercentrics Apps'),
      '#header' => [
        $this->t('Service'),
        $this->t('Usercentrics ID'),
        $this->t('Enabled'),
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'apps-weight',
        ],
      ],
    ];

    // Display table of apps.
    foreach ($apps as $app) {
      /** @var \Drupal\usercentrics\Entity\UsercentricsApp $app */
      $operations = $list_builder->getOperations($app);

      $form['apps'][$app->Id()] = [
        '#attributes' => ['class' => ['draggable']],
        'title' => [
          '#plain_text' => $app->label(),
        ],
        'menu_name' => [
          '#plain_text' => $app->ucId(),
        ],
        'enabled' => [
          '#type' => 'checkbox',
          '#default_value' => $app->status(),
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => $app->label()]),
          '#title_display' => 'invisible',
          '#default_value' => $app->weight(),
          '#delta' => ceil(count($apps) / 2),
          '#attributes' => ['class' => ['apps-weight']],
        ],
        'operations' => [
          '#type' => 'dropbutton',
          '#links' => $operations,
        ],
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    // By default, render the form using theme_system_config_form().
    $form['#theme'] = 'system_config_form';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('usercentrics_app');
    $values = $form_state->getValue('apps');
    $apps = $storage->loadMultiple(array_keys($values));

    foreach ($apps as $app) {
      $value = $values[$app->id()];
      $app->setStatus((bool) $value['enabled']);
      $app->setWeight((float) $value['weight']);
      $storage->save($app);
    }

    $this->messenger->addMessage($this->t('The new apps ordering has been applied.'));
  }

}
