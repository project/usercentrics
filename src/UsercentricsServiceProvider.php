<?php

namespace Drupal\usercentrics;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the JsCollectionRenderer service.
 */
class UsercentricsServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('asset.js.collection_renderer');
    $definition->setClass('Drupal\usercentrics\UsercentricsJsCollectionRenderer');
  }

}
